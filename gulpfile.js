'use strict';

console.log('------------------------------ Hello!', new Date().getUTCDate());

const projectName = "TGG_GYM_MASTER";
const imagePorjectID = "_gyn_M_";

const gulp = require('gulp');
const fs = require('fs');
const path = require('path');
const browserSync = require('browser-sync').create();
const less = require('gulp-less');
const sass = require('gulp-sass');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const gulpIf = require('gulp-if');
const image = require('gulp-image');
const rename = require('gulp-rename');
const cleanCSS = require('gulp-clean-css');
const del = require('del');
const bump = require('gulp-bump');
const zip = require('gulp-zip');
const size = require('gulp-size');
const useref = require('gulp-useref');
const gulpVersionNumber = require("gulp-version-number");
const cssnano = require('gulp-cssnano');
const glob = require('glob');
const gulpLoadPlugins = require('gulp-load-plugins');
const $ = gulpLoadPlugins();
const replace = require('gulp-replace');
const filter = require("gulp-filter");

const dom = require('gulp-dom');

const paths = {
    src: 'src',
    dist: 'dist',
    dataSrc: 'src/dataSrc'
};


// test to determine, if the directory describes a size of a banner
const isSizeDirectory = dir => (/\d{2,3}x\d{2,3}$/g).test(dir);

// all files
const allFiles = glob.sync(`${paths.src}/**/*`);

const allSrcFolders =  glob.sync(`${paths.dataSrc}/*`);


// console.log("allLanguageFiles== ", allLanguageFiles);


/**
  Finds the directory specifying width and height and inform about the path to this directory
*/



function srcPath(path) {
    var obj = {};

    obj.arr = path.split('/');
    obj.dir = obj.arr[obj.arr.length - 1];
    obj.location = obj.arr[obj.arr.length - 2];
    obj.path = obj.arr.join("/");
    obj.copySrc =  obj.dir + ".json";
    obj.imageSrc =  obj.path + "/images";

    return obj;
}



function clean() {
    // You can use multiple globbing patterns as you would with `gulp.src`,
    // for example if you are using del 2.0 or above, return its promise
    return del(['dist']);
}


function bsync() {
    browserSync.init({
        server: {
            baseDir: 'src'
        },
    })
}

function bsyncTest() {
    browserSync.init({
        server: {
            baseDir: 'dist'
        },
    })
}



async function copyAllImages() {
    
    console.log("copyAllImages== ");
     
    allSrcFolders.forEach((dir) => {

        var fileObj = srcPath(dir);
       return gulp.src(dir+'/images/**/*.+(png|jpg|gif|svg)')
           .pipe(image({
                pngquant: false,
                optipng: false,
                zopflipng: false,
                jpegRecompress: false,
                mozjpeg: false,
                guetzli: false,
                gifsicle: false,
                svgo: false,
                concurrent: 10,
                quiet: false // defaults to false		
            }))
          .pipe(gulp.dest(paths.dist + "/" + fileObj.dir + '/images'));
        
        });
        
}


async function copyStyles() {
    
    var stylesPath =  paths.src + '/HTML/styles/*';
     console.log("copyStyles== ");
    allSrcFolders.forEach((dir) => {

       var fileObj = srcPath(dir);
        // console.log("copyAllImages== ", dir+'/images/' );
       return gulp.src(stylesPath)
          .pipe(gulp.dest(paths.dist + "/" + fileObj.dir + '/styles'));
        
        });
        
}

async function copyInject() {
    var htmlTemplate =  paths.src + '/HTML/*.html';
    
   //  console.log("allLanguageFiles== ", htmlTemplate);
    
    console.log("copyInject== ");
    
    allSrcFolders.forEach((langDir) => {

        var fileObj = srcPath(langDir);
        var languageName= fileObj.dir;
        var json = fs.readFileSync(fileObj.path + '/'+ fileObj.copySrc, "utf8");
        var jsonData = JSON.parse(json);
        var nodeIdArray = Object.keys(jsonData[0]);
        var nodeCopyArray = Object.entries(jsonData[0]);
        
        //  console.log("allLanguageFiles== ", htmlTemplate );
        
            let task = gulp.src(htmlTemplate);
              Object.keys(jsonData[0]).forEach((key) => {
                 // console.log("allLanguageKey==  ",  jsonData[0][key])
                task = task.pipe(replace('#{'+ key +'}#', jsonData[0][key]));
              });
            return task.pipe(gulp.dest(paths.dist + "/" + languageName+ '/'))
    });
}


//var build = gulp.series(clean, gulp.parallel(copyInject));
var build = gulp.series(clean,copyInject,copyAllImages,copyStyles);
//var build = gulp.series(clean,copyAllImages);


exports.copyAllImages = copyAllImages; 
exports.clean = clean;
exports.bsync = bsync;
exports.copyInject =copyInject;
exports.build = build;



exports.default = build;