# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* It take in JSON files with copy language translations and publishes HTML files accordingly  
* Version 1


### How do I get set up? ###

* install Node  
* install nvm: curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
* install Node vertion 10.5.0 >> nvm install 10.5.0 
* npm 6.1.0
* sudo npm install --global gulp-cli
* npm install --global gulp-cli
* git clone https://superalitos@bitbucket.org/superalitos/translationtool.git
* npm install 

### Folder structure ###

* src/HTML: home to the html template. (Inclduing images and styles folder).
* src/dataSrc/ home to language data master folder DE,FR, etc
* src/dataSrc/DE: make sure to include DE.json file (with language translation) + an images folder containing all relevant (translated) images.

* in src/HTML template: replace each copy with the relevant id/name node outlined in the JSON file. To avoid any conflics with existing copy, wrap the id within #{..}# ... for example #{copy_1}#     

* run 'gulp' from terminal  
 
### The process: 'gulp' run command ###

* 1- creates a new folder for each language in dist/DE,FR etc..   
* 2- Takes the translation from the JSON file and repaces the marked  #{..}# ids with the relevant copy.  
    
    NOTE: the copy within the JSON file can be HTML markup to allow links, special characters and font styling. 
    
    Example: 
    
    "Para_2_3": "An all-screen Liquid Retina display<sup class='footnote'><a href='#footnote-1' aria-label='Footnote 1'>1</a></sup> with ProMotion and True&nbsp;Tone makes everything feel&nbsp;as&nbsp;magical as it looks.", 
    

###  Expected results ###

* a new language folder 'dist/DE,FR,etc' will be created including the translated HTML + images and styles folders   

###  Testing ###

* make sure to have HTML + images + Styles within each created language folder 
* run the HTML in browser and lookout for copy and images updates


###  Useful commands ###

* nvm use 10.5.0 >> to target an specific node version 
* npm -v >> 6.1.0
* gulp -v  >> CLI v: 2.3.0   .... local v: 4.0.2  
 

### Who do I talk to? ###

* Alexander Tobias atobiasink@gmail.com






